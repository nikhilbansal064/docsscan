package com.example.docscan.DataModel

import com.example.docscan.Utils.DocumentType
import java.util.*

data class Document(
    val documentPath: String,
    val documentName: String,
    val documentDate: Date,
    val documentType: DocumentType
)