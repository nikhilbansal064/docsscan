package com.example.docscan.Utils

import com.example.docscan.R

enum class DocumentType(val icon: Int) {
    PDF(R.drawable.ic_samples_svgrepo_com),
    IMAGE(R.drawable.ic_samples_svgrepo_com)
}