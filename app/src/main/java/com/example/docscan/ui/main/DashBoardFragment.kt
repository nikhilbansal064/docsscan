package com.example.docscan.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.docscan.DataModel.Document
import com.example.docscan.R
import kotlinx.android.synthetic.main.dashboard_fragment.*
import kotlinx.android.synthetic.main.document_list_header.*
import kotlinx.android.synthetic.main.document_list_header.view.*
import kotlinx.android.synthetic.main.document_list_header.view.grid_layout_icon

class DashBoardFragment : Fragment() {


    companion object {
        fun newInstance() = DashBoardFragment()
    }

    private lateinit var viewModel: DashBoardViewModel
    private lateinit var documentListAdapter: DocumentListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.dashboard_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DashBoardViewModel::class.java)
        documentListAdapter = DocumentListAdapter(emptyList())
        setUpUI()

        viewModel.documentLiveData?.observe(viewLifecycleOwner, Observer<List<Document>> {
            documentListAdapter?.updateDocumentList(it)
            updateUiVisibility()
            setUpRecyclerView(documentListAdapter.layoutStyle)
        })
    }

    private fun setUpUI() {

        updateUiVisibility()
        setUpRecyclerView()

        list_header.list_layout_icon.setOnClickListener {
            setUpRecyclerView(DocumentListAdapter.LAYOUT_STYLE_LIST)
        }

        grid_layout_icon.setOnClickListener {
            setUpRecyclerView(DocumentListAdapter.LAYOUT_STYLE_GRID)
        }

        new_scan_button.setOnClickListener { view ->
            view.findNavController().navigate(DashBoardFragmentDirections.actionDashBoardFragmentToCameraPreviewFragment())

        }
    }

    private fun updateUiVisibility() {
        if (documentListAdapter?.itemCount > 0) {
            document_list.visibility = View.VISIBLE
            list_header.visibility = View.VISIBLE
            //scan_document_badge_view.visibility = View.GONE
            no_document_available_layout.visibility = View.GONE

        } else{
            document_list.visibility = View.GONE
            list_header.visibility = View.GONE
            //scan_document_badge_view.visibility = View.VISIBLE
            no_document_available_layout.visibility = View.VISIBLE
        }


    }

    private fun setUpRecyclerView(layoutStyle: Int = DocumentListAdapter.LAYOUT_STYLE_LIST) {
        document_list?.apply {
            layoutManager = if (layoutStyle == DocumentListAdapter.LAYOUT_STYLE_LIST)
                LinearLayoutManager(context, RecyclerView.VERTICAL, false) else getGridLayoutManager()

            documentListAdapter.updateLayoutStyle(layoutStyle)
            document_list.adapter = documentListAdapter
        }
    }

    fun getGridLayoutManager(): GridLayoutManager {
        return GridLayoutManager(context, 2)
    }

}
