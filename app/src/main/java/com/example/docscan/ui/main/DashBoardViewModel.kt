package com.example.docscan.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.docscan.DataModel.Document
import com.example.docscan.Utils.DocumentType
import java.util.*
import kotlin.collections.ArrayList


class DashBoardViewModel : ViewModel() {
    var documentLiveData: MutableLiveData<ArrayList<Document>>? = MutableLiveData()
    var documentlist: ArrayList<Document> = ArrayList()

    init {
        PopulateDummyArrayList()
        documentLiveData?.value = documentlist
    }



    private fun PopulateDummyArrayList() {
        documentlist.apply {
            add(Document("cscscs", "AdhharCard", Date(), DocumentType.IMAGE))
            add(Document("cscscs", "AdhharCardw", Date(), DocumentType.PDF))
            add(Document("cscscs", "AdhharCardw", Date(), DocumentType.PDF))
        }

    }
}
