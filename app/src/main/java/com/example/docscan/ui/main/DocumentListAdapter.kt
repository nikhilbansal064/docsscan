package com.example.docscan.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.docscan.DataModel.Document
import com.example.docscan.R
import java.util.*

class DocumentListAdapter(var documentList: List<Document>, var layoutStyle: Int = LAYOUT_STYLE_LIST): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layout = if (layoutStyle == LAYOUT_STYLE_GRID) {
            return DocumentListGridItemVH(LayoutInflater.from(parent.context).inflate(R.layout.document_list_grid_item,
                parent,
                false))

        } else {
            return DocumentListGridItemVH(LayoutInflater.from(parent.context).inflate(R.layout.document_list_item,
                parent,
                false))

        }
    }

    override fun getItemCount(): Int {
        return documentList.size
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        documentList[position]?.let {
            (holder as DocumentListGridItemVH).bind(it)
        }
    }

    private fun getFormatDate(documentDate: Date): String {
        //TODO - need to move in utils class
        return "21/10/1992"
    }

    fun updateDocumentList(documentList: List<Document>?){
        this.documentList = documentList ?: emptyList()
        notifyDataSetChanged()
    }

    fun updateLayoutStyle(layoutStyle: Int) {
        this.layoutStyle = layoutStyle
    }



    companion object {
        const val TYPE_HEADER = 1
        const val TYPE_ITEM = 2

        const val LAYOUT_STYLE_GRID = 1
        const val LAYOUT_STYLE_LIST = 2
    }
}