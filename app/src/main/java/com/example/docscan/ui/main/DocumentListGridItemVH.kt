package com.example.docscan.ui.main

import android.view.View

import androidx.recyclerview.widget.RecyclerView
import com.example.docscan.DataModel.Document
import kotlinx.android.synthetic.main.document_list_grid_item.view.*
import java.util.*

class DocumentListGridItemVH(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bind(document: Document) {
        itemView.item_name.text = document.documentName
        itemView.item_creation_date.text = getFormatDate(document.documentDate)
        itemView.item_type_icon.setImageResource(document.documentType.icon)
    }

    private fun getFormatDate(documentDate: Date): CharSequence? {
        return "21/12/2002"
    }
}